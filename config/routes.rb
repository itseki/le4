Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  delete 'posts/:id', to: 'posts#destroy'
  resources :posts
  root 'posts#index'
end
